<!-- BEGIN plugin-resources/webapp/xnat-templates/screens/Scripts.vm -->
#* @vtlvariable name="turbineUtils" type="org.nrg.xdat.turbine.utils.TurbineUtils" *#
#* @vtlvariable name="siteConfig" type="java.util.Properties" *#
#* @vtlvariable name="content" type="org.apache.turbine.services.pull.tools.ContentTool" *#
#* @vtlvariable name="data" type="org.apache.turbine.util.RunData" *#
#* @vtlvariable name="tabs" type="java.util.List<java.util.Properties>" *#
#* @vtlvariable name="scripts" type="java.util.List<org.nrg.automation.entities.Script>" *#
#* @vtlvariable name="script" type="org.nrg.automation.entities.Script" *#
#* @vtlvariable name="link" type="org.apache.turbine.util.template.TemplateLink" *#
#* @vtlvariable name="error" type="java.lang.String" *#


<h3 style="margin:0 0 15px 0;">Automation</h3>


<div class="alert" style="margin:15px 0;max-width:696px;min-width:400px;">
    Automation is an emerging feature in the XNAT server platform. It allows you to create scripts in a number
    of scripting languages that can be triggered by system events. You should use it only if you or your team
    has a thorough understanding of XNAT, your system architecture and environment, and the particular
    scripting languages involved. Refer to the
    <a href="https://wiki.xnat.org/display/XNAT16/XNAT+Automation" target="_blank">XNAT Automation documentation</a>
    for more information. For specific questions, you can visit the
##    <a href="https://wiki.xnat.org/display/XNAT16/XNAT+Automation+Discussion" target="_blank">XNAT Automation Discussion forum</a> or the
    <a href="https://groups.google.com/forum/#!forum/xnat_discussion" target="_blank">XNAT discussion group</a>.
</div>


<div class="yui-skin-sam">

    <div id="tp_fm" style="display:none"></div>

    #if($data.getSession().getAttribute("user").checkRole("Administrator"))

        <style type="text/css">
            #automation-events-scripts .yui-content > div { padding: 2px; }
            td.edit:hover { cursor: pointer }
            td.edit:hover a,
            td.actions a:hover { text-decoration: underline }
            td.edit a,
            td.actions a { display: inline-block; padding: 2px 8px; }
            table.edit-script { margin-bottom: -4px; }
            table.edit-script td { padding: 4px 2px; vertical-align: baseline; }
        </style>

        <script type="text/javascript" src="$content.getURI("scripts/xnat/app/siteEventsManager.js")"></script>

        <div id="addEventHandler" class="html-template">
            <table>
                <tr>
                    <td><label for="event" class="required"><strong>Event:</strong><i>*</i></label></td>
                    <td><select id="select_event" name="event" class="event"></select></td>
                </tr>
                <tr>
                    <td><label for="scriptId" class="required"><strong>Script ID:</strong><i>*</i></label></td>
                    <td><select id="select_scriptId" name="scriptId" class="scriptId"></select></td>
                </tr>
                <tr>
                    <td><label for="description"><strong>Description:</strong></label></td>
                    <td><input id="description" name="description" class="description" type="text" size="40"></td>
                </tr>
            </table>
        </div>

        <!-- SITE EVENTS TABLE ROW TEMPLATE -->
        <table id="event-row-template" class="html-template">
            <tbody>
            <tr class="highlight events-list"
##                    data-workflow-label="__WORKFLOW_LABEL__"
                    data-event-id="__SITE_EVENT_ID__"
                    data-event-label="__SITE_EVENT_LABEL__">
                <td class="site-event-id">
##                    <a href="#!" class="site-event-id"><b>
                        __SITE_EVENT_ID__
##                    </b></a>
                </td>
                <td class="site-event-label">
##                    <a href="#!" class="site-event-label">
                        __SITE_EVENT_LABEL__
##                    </a>
                </td>
##                <td class="workflow-label">__WORKFLOW_LABEL__</td>
                <td class="actions" style="text-align:center;white-space:nowrap;">
##                    <a href="#!" data-action="editEvent" title="edit existing event">edit</a>
                    <a href="#!" class="delete-event" data-action="deleteEvent" title="delete existing event">delete</a>
                </td>
            </tr>
            </tbody>
        </table>

        <table id="script-row-template" class="html-template">
            <tbody>
            <tr class="highlight script-list" data-script-id="__SCRIPT_ID__">
                <td class="edit" data-action="editScript">
                    <a href="#!" class="edit"><b>__SCRIPT_ID__</b></a>
                </td>
                <td class="edit" data-action="editScript">__SCRIPT_DESCRIPTION__</td>
                <td class="actions" style="text-align:center;white-space:nowrap;">
                    <a href="#!" data-action="editScript" title="edit existing script">edit</a>
                    <a href="#!" data-action="deleteScript" title="delete existing script">delete</a>
                    <a href="#!" data-action="duplicateScript" title="copy existing script to a new script">duplicate</a>
                </td>
            </tr>
            </tbody>
        </table>

        <div id="automation-events-scripts" class="yui-module">
            <div class="yui-navset yui-navset-top bogus-tabs">
                <ul class="yui-nav">
                    <li class="first selected"><a href="#automationEventHandlers"><em>Site Event Handlers</em></a></li>
                    <li><a href="#automationEvents"><em>Events</em></a></li>
                    <li><a href="#automationScripts"><em>Scripts</em></a></li>
                </ul>
                <div class="yui-content">


                    <!-- Site Event Handlers -->
                    <div id="automationEventHandlers">

##                        <h3>Event Handlers</h3>
                        <div id="events_list">
##                            <p id="no_events_defined" style="display:none;padding:20px;">There are no events currently defined for this site.</p>
                            <p id="no_event_handlers" style="display:none;padding:20px;">There are no event handlers currently configured for this site.</p>
                            <table id="events_table" class="xnat-table" style="display:none;width:100%;">
                                <thead>
                                <th>Event</th>
                                <th>Script ID</th>
                                <th>Description</th>
                                <th></th>
                                </thead>
                                <tbody>
                                ## content populated with XNAT.app.eventsManager.initEventsTable()
                                </tbody>
                            </table>
                            <br>
                            <b style="padding:0 8px;">Create a site-wide Event Handler: </b>
                            <button type="button" id="add_event_handler" class="btn1" style="font-size:12px;" title="add an event handler">Add Event Handler</button>
                        </div>


                    </div>

                    <!-- Events -->
                    <div id="automationEvents" class="yui-hidden">

                        <div id="events-container">

                            <p id="no-events" style="display:none;">There are no Events defined for this site.</p>

                            <table id="events-table" class="xnat-table" style="display:none;width:100%;">

                                <thead>
                                <th width="40%">Event ID</th>
                                <th width="40%">Event Label</th>
##                                <th>Workflow</th>
                                <th width="20%">&nbsp;</th>
                                </thead>

                                <tbody>

                                ## events list inserted here via AJAX

                                </tbody>

                            </table>

                            <br>

                            <b style="padding:0 8px;">Define an Event from existing Workflows or create a new Event:</b>

                            <button type="button" id="define-event-button" class="btn1" style="font-size:12px;" title="define an event">Define Event</button>

                        </div>

                    </div>

                    <!-- Scripts -->
                    <div id="automationScripts" class="yui-hidden">
                        <!-- SCRIPTS LIST TABLE -->
                        <div id="scripts-container">

                            <p id="no-scripts-installed" style="display:none;">No scripts are currently installed on this system.</p>

                            <table id="scripts-table" class="xnat-table" style="display:none;width:100%;">
                                <thead>
                                <th>Script ID</th>
                                <th width="50%">Description</th>
                                ##                <th>Version</th>
                                <th>&nbsp;</th>
                                ##                <th>&nbsp;</th>
                                ##                <th>&nbsp;</th>
                                </thead>
                                <tbody>

                                ## script list inserted here via AJAX

                                </tbody>
                            </table>

                            <br>

                            <b style="padding:0 8px;">Add a new script: </b>

                            <select id="add-script-language" class="language">
                                <option value="!">Select a Language</option>
                            </select>

                            <button type="button" id="add-script-button" class="btn1" style="font-size:12px;" title="add new script">Add Script</button>

                        </div>
                    </div>

                </div>
            </div>
        </div>

        <script type="text/javascript">

            // YUI tab simulator using jQuery for event handling for '.bogus-tabs'
            // using 'on' to handle any tabs that may be added dynamically
            jq('.yui-navset.bogus-tabs').each(function(){

                var __navset = jq(this),
                    TABS = '> ul.yui-nav > li',
                    CONTENTS = '> .yui-content > div',
                    selected = null;

                __navset.on('click', TABS + '> a', function(e){

                    e.preventDefault();

                    var __link = jq(this),
                        __tab = __link.closest('li');

                    selected = __link.attr('href'); // safer to use jQuery to get 'href' value

                    __navset.find(TABS).removeClass('selected');
                    __tab.addClass('selected');

                    __navset.find(CONTENTS).addClass('yui-hidden');
                    __navset.find(selected).removeClass('yui-hidden');

                });

            });

        </script>


        <!-- SITE EVENTS DIALOG TEMPLATE -->
        <div id="site-events-template" class="html-template">
            <p>Select a Workflow or enter a new Event name to define an Event to use with project and site Event Handlers:</p>
            <table>
                <tr>
                    <td><label for="workflow-event-id-menu" class="required"><strong>Workflow:</strong><i>*</i></label></td>
                    <td>

                        <div id="workflow-event-id-container">
                        ## workflow events menu
                        </div>
##                        <select id="workflow-event-id-menu" name="event_id" class="event_id active">
##                            <option data-label="__WORKFLOW_EVENT_LABEL__" data-id="__WORKFLOW_EVENT_ID__" value="__WORKFLOW_EVENT_ID__">__WORKFLOW_EVENT_LABEL__</option>
##                        </select>

                    </td>
                </tr>
                <tr>
                    <td> </td>
                    <td>
                        <div id="custom-event-id" style="padding:10px 0;">
                            <input type="checkbox" id="custom-event-checkbox">
                            <label for="custom-event-checkbox">Or enter a custom Event id:</label>
                            <br>
                            <input type="text" id="custom-event-input" class="event_id" placeholder="(enter custom Event id)" disabled>
                        </div>
                    </td>
                </tr>
##                <tr>
##                    <td><label for="scriptId" class="required"><strong>Script ID:</strong><i>*</i></label></td>
##                    <td><select name="scriptId" class="scriptId"></select></td>
##                </tr>
                <tr>
                    <td><label for="events-event-label"><strong>Label:</strong></label></td>
                    <td>
                        <input id="events-event-label" name="event_label" class="event_label" type="text" size="40">
                    </td>
                </tr>
                <tr>
                    <td> </td>
                    <td>
                        <small>Label not required. If no label is entered, it will be the same as the ID.</small>
                    </td>
                </tr>
            </table>
        </div>



        <!-- SCRIPT EDITOR TEMPLATE -->
        <div id="script-editor-template" class="html-template">
            <input type="hidden" name="id" class="id" value="">
            <input type="hidden" name="scriptId" class="scriptId" value="">
            <input type="hidden" name="language" class="language" value="">
            <input type="hidden" name="languageVersion" class="languageVersion" value="">
            <input type="hidden" name="timestamp" class="timestamp" value="">
            <table class="edit-script" border="0" cellspacing="0">
                <tr>
                    <td><b>Script ID: </b>&nbsp;</td>
                    <td>
                        <b class="script-id-text"></b>
                        <input type="text" name="script-id-input" class="script-id-input" size="30" value="">
                    </td>
                </tr>
                <tr>
                    <td><b>Description: </b>&nbsp;</td>
                    <td><input type="text" name="script-description" class="script-description" size="80" value=""></td>
                </tr>
            </table>
            <br>
            <div style="width:840px;height:482px;position:relative;">
                <div class="editor-content" style="position:absolute;top:0;right:0;bottom:0;left:0;border:1px solid #ccc;"></div>
            </div>
        </div><!-- /#script-editor-template -->

        <script type="text/javascript" src="$content.getURI("scripts/ace/ace.js")"></script>
        <script type="text/javascript" src="$content.getURI("scripts/xnat/app/automation.js")"></script>
        <script type="text/javascript" src="$content.getURI("scripts/xnat/app/scriptEditor.js")"></script>

    #else
        <div id="congratsContainer" class="container" style="width:98%;margin:5px;background-color:#797979;">
            <div id="congratsBody" class="containerBody">
                <div class="containerItem edit_header2" style="white-space:normal;line-height:18px;">Security Warning</div>
                <div class="containerItem" style="white-space:normal;">You do not have administrator access and can't access this page.</div>
            </div>
        </div>
    #end

    <!-- END plugin-resources/webapp/xnat-templates/screens/Scripts.vm -->
